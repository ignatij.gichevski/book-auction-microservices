#!/bin/bash

cd book-auction-eureka-registry
mvn package docker:build
cd ..

cd book-auction-gateway
mvn package docker:build
cd ..

cd book-auction-zuul
mvn package docker:build
cd ..

cd login
mvn package docker:build
cd ..

cd notification
mvn package docker:build
cd ..

cd offer
mvn package docker:build
cd ..

docker-compose up > log.txt
