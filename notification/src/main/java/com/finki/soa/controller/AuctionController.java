package com.finki.soa.controller;

import com.finki.soa.model.Book;
import com.finki.soa.model.Notification;
import com.finki.soa.model.Offer;
import com.finki.soa.model.Person;
import com.finki.soa.repository.BookRepository;
import com.finki.soa.repository.OfferRepository;
import com.finki.soa.repository.PersonRepository;
import com.finki.soa.service.BookService;
import com.finki.soa.service.NotificationService;
import com.finki.soa.service.OfferService;
import com.finki.soa.service.PersonService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

import javax.servlet.http.HttpServletRequest;

@RestController
public class AuctionController {

	@Autowired
	private PersonService personService;

	@Autowired
	private BookService bookService;

	@Autowired
	private OfferService offerService;

	@Autowired
	private NotificationService notificationService;

	@GetMapping("/book")
	public List<Book> getAllBooks() {
		return bookService.findAll();
	}

	@GetMapping("/book/name/{name}")
	public Book getBookByName(@PathVariable String name) {
		return bookService.findBookByName(name);
	}

	@GetMapping("/person")
	public Iterable<Person> getPersons() {
		return personService.findAll();
	}

	@GetMapping("/person/{id}")
	public Person getPerson(@PathVariable Long id) {
		return personService.findOne(id);
	}

	@GetMapping("/person/name/{firstName}/{secondName}")
	public List<Person> findPersonByFirstAndSecondName(@PathVariable String firstName,
			@PathVariable String secondName) {
		return personService.findByFirstNameAndSecondName(firstName, secondName);
	}

	@GetMapping("/finish")
	public void finishAuction() {
		Set<Book> books = offerService.findAllBooksUsedInOffers();
		books.forEach(book -> {
			List<Person> usersToBeNotified = new ArrayList<>();
			List<Offer> offersByBook = offerService.findAllByBook(book);
			offersByBook.sort((Offer o1, Offer o2) -> o1.getMoney() - o2.getMoney());
			offersByBook.subList(0, offersByBook.size() - 1).stream().map(Offer::getPerson).forEach(person -> {
				usersToBeNotified.add(person);
			});
			// notify users that they lost the battle for the book
			notificationService.notifyUsersWhoLost(book, usersToBeNotified);
			// set the winner to be the owner of the book
			Person owner = offersByBook.get(offersByBook.size() - 1).getPerson();
			book.setOwner(owner);
			bookService.save(book);
			//notify winner
			notificationService.notifyWinner(book,  owner);
		});
	}

}
