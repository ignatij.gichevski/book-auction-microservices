package com.finki.soa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finki.soa.model.Book;
import com.finki.soa.repository.BookRepository;

@Service
public class BookService {

	@Autowired
	private BookRepository bookRepository;

	public Book findBookByName(String bookName) {
		return bookRepository.findByName(bookName);
	}

	public List<Book> findAll() {
		return bookRepository.findAll();
	}

	public void save(Book b) {
		bookRepository.save(b);
	}

}
